////////////////////////////////////////////////////////////////////////////////////////////////////////////////
//                                                                                                            //
//  BITHELPERS - additional bit manipulation for qword, longword, word, byte and boolean types                //
//                                                                                                            //
//  Made by Zeljko Avramovic (user avra in Lazarus forum)                                                     //
//                                                                                                            //
//  This very basic unit is released under 3 licenses: 1) LGPL3, 2) FPC modified LGPL, 3) BSD3. That should   //
//  cover most cases that I can think of. First was chosen to be compatible with original Settimino library.  //
//  Second was chosen for full compatibility with FreePascal/Lazarus, and the third for every other case.     //
//  Yes, in short this means that commercial usage is allowed and encouraged.                                 //
//                                                                                                            //
////////////////////////////////////////////////////////////////////////////////////////////////////////////////

unit bithelpers;

{$mode objfpc}
{$modeswitch typehelpers}

interface

uses
  Classes, SysUtils;

const
  lzShowLeadingZeros = true;
  lzHideLeadingZeros = false;

  BitValueLookupTable: array[0..63] of QWord = ( 1, 2, 4, 8, 16, 32, 64, 128,
                                                 256, 512, 1024, 2048, 4096, 8192, 16384, 32768,
                                                 65536, 131072, 262144, 524288, 1048576, 2097152, 4194304, 8388608,
                                                 16777216, 33554432, 67108864, 134217728, 268435456, 536870912, 1073741824, 2147483648,
                                                 4294967296, 8589934592, 17179869184, 34359738368, 68719476736, 137438953472, 274877906944, 549755813888,
                                                 1099511627776, 2199023255552, 4398046511104, 8796093022208, 17592186044416, 35184372088832, 70368744177664, 140737488355328,
                                                 281474976710656, 562949953421312, 1125899906842624, 2251799813685248, 4503599627370496, 9007199254740992, 18014398509481984, 36028797018963968,
                                                 72057594037927936, 144115188075855872, 288230376151711744, 576460752303423488, 1152921504606846976, 2305843009213693952, 4611686018427387904, 9223372036854775808 );

type
  THelperFormatSettings = record
    PrefixString:      string;
    SufixString:       string;
    NibbleSeparator:   string;
    ByteSeparator:     string;
    WordSeparator:     string;
    LongwordSeparator: string;
  end;

var
  BIT_ON_TXT: string    = 'On';
  BIT_OFF_TXT: string   = 'Off';

  BIT_TRUE_TXT: string  = 'True';
  BIT_FALSE_TXT: string = 'False';

  BIT_ONE_TXT: char   = '1';
  BIT_ZERO_TXT: char  = '0';

  DefaultHelperFormatSettings : THelperFormatSettings = (
    PrefixString:      '';
    SufixString:       '';
    NibbleSeparator:   '';
    ByteSeparator:     '';
    WordSeparator:     '';
    LongwordSeparator: '';
  );

  //HelperFormatSettings: THelperFormatSettings absolute DefaultHelperFormatSettings;

type

  ///////////////////////
  // TBooleanBitHelper //
  ///////////////////////

  TStringCaseFormat = (scfUnchangedCase, scfLowerCase, scfUpperCase);

  TBooleanBitHelper = type helper(TBooleanHelper) for Boolean
  public
    function ToString(const aBit: boolean; const aTrueStr, aFalseStr: string; const aCharsCase: TStringCaseFormat = scfUnchangedCase): string; overload;
    function ToString(const aTrueStr, aFalseStr: string; const aCharsCase: TStringCaseFormat = scfUnchangedCase): string; overload;
    function ToOneZeroString: string;
    function ToOnOffString(const aCharsCase: TStringCaseFormat = scfUnchangedCase): string;
    function ToTrueFalseString(const aCharsCase: TStringCaseFormat = scfUnchangedCase): string;
  end;

  ////////////////////
  // TByteBitHelper //
  ////////////////////

  TByteBits = 0..7;

  TByteOverlay = bitpacked record case integer of     // for fast extraction of bits
    0: (AsBit:  bitpacked array[TByteBits] of boolean);
    1: (AsByte: byte);
  end;

  TByteBitHelper = type helper(TByteHelper) for Byte
  const
    MinBit = Low(TByteBits);
    MaxBit = High(TByteBits);
  public
    procedure Clear;
    function  ToBinString(const HelperFormatSettings: THelperFormatSettings; const aShowLeadingZeros: boolean = true): string; overload;
    function  ToBinString(const aShowLeadingZeros: boolean = true): string; overload;
    function  HighestBitPos: cardinal; inline;
    function  LowestBitPos: cardinal; inline;
    function  BitsCount: byte; inline;
    function  GetBit(const aIndex: TByteBits): boolean;
    procedure SetBit(const aIndex: TByteBits; const NewValue: boolean);
    property  Bit[aIndex: TByteBits]: boolean read GetBit write SetBit;
  end;

  ////////////////////
  // TWordBitHelper //
  ////////////////////

  TWordBits  = 0..15;
  TWordBytes = 0..1;

  TWordOverlay = bitpacked record case integer of     // for fast extraction of bytes and bits
    0: (AsBit:         bitpacked array[TWordBits] of boolean);
    1: (AsByte:        array[TWordBytes] of byte);
    2: (AsWord:        word);
    // recursive overlay:
    3: (AsByteOverlay: array[TWordBytes] of TByteOverlay);
  end;

  TWordBitHelper = type helper(TWordHelper) for Word
  const
    MinBit  = Low(TWordBits);
    MaxBit  = High(TWordBits);
    MinByte = Low(TWordBytes);
    MaxByte = High(TWordBytes);
  public
    procedure Clear;
    function  ToBinString(const HelperFormatSettings: THelperFormatSettings; const aShowLeadingZeros: boolean = true): string; overload;
    function  ToBinString(const aShowLeadingZeros: boolean = true): string; overload;
    function  HighestBitPos: cardinal; inline;
    function  LowestBitPos: cardinal; inline;
    function  BitsCount: byte; inline;
    function  GetBit(const aIndex: TWordBits): boolean;
    procedure SetBit(const aIndex: TWordBits; const NewValue: boolean);
    property  Bit[aIndex: TWordBits]: boolean read GetBit write SetBit;
    function  GetByte(const aIndex: TWordBytes): byte;
    procedure SetByte(const aIndex: TWordBytes; const NewValue: byte);
    property  Byte[aIndex: TWordBytes]: byte read GetByte write SetByte;
  end;

  ////////////////////////
  // TLongwordBitHelper //
  ////////////////////////

  TLongwordBits  = 0..31;
  TLongwordBytes = 0..3;
  TLongwordWords = 0..1;

  TLongwordOverlay = bitpacked record case integer of     // for fast extraction of words, bytes and bits
    0: (AsBit:         bitpacked array[TLongwordBits] of boolean);
    1: (AsByte:        array[TLongwordBytes] of byte);
    2: (AsWord:        array[TLongwordWords] of word);
    3: (AsLongword:    longword);
    // recursive overlays:
    4: (AsByteOverlay: array[TLongwordBytes] of TByteOverlay);
    5: (AsWordOverlay: array[TLongwordWords] of TWordOverlay);
  end;

  TLongwordBitHelper = type helper(TCardinalHelper) for cardinal
  const
    MinBit  = Low(TLongwordBits);
    MaxBit  = High(TLongwordBits);
    MinByte = Low(TLongwordBytes);
    MaxByte = High(TLongwordBytes);
    MinWord = Low(TLongwordWords);
    MaxWord = High(TLongwordWords);
  public
    procedure Clear;
    function  ToBinString(const HelperFormatSettings: THelperFormatSettings; const aShowLeadingZeros: boolean = true): string; overload;
    function  ToBinString(const aShowLeadingZeros: boolean = true): string; overload;
    function  HighestBitPos: cardinal; inline;
    function  LowestBitPos: cardinal; inline;
    function  BitsCount: byte; inline;
    function  GetBit(const aIndex: TLongwordBits): boolean;
    procedure SetBit(const aIndex: TLongwordBits; const NewValue: boolean);
    property  Bit[aIndex: TWordBits]: boolean read GetBit write SetBit;
    function  GetByte(const aIndex: TLongwordBytes): byte;
    procedure SetByte(const aIndex: TLongwordBytes; const NewValue: byte);
    property  Byte[aIndex: TLongwordBytes]: byte read GetByte write SetByte;
    function  GetWord(const aIndex: TLongwordWords): word;
    procedure SetWord(const aIndex: TLongwordWords; const NewValue: word);
    property  Word[aIndex: TLongwordWords]: word read GetWord write SetWord;
  end;

  ////////////////////////
  // TQuadwordBitHelper //
  ////////////////////////

  quadword = qword; // alias

  TQuadwordBits      = 0..63;
  TQuadwordBytes     = 0..7;
  TQuadwordWords     = 0..3;
  TQuadwordLongwords = 0..1;

  TQuadwordOverlay = bitpacked record case integer of     // for fast extraction of longwords, words, bytes and bits
    0: (AsBit:             bitpacked array[TQuadwordBits] of boolean);
    1: (AsByte:            array[TQuadwordBytes] of byte);
    2: (AsWord:            array[TQuadwordWords] of word);
    3: (AsLongword:        array[TQuadwordLongwords] of longword);
    4: (AsQuadword:        qword);
    // recursive overlays:
    5: (AsByteOverlay:     array[TQuadwordBytes] of TByteOverlay);
    6: (AsWordOverlay:     array[TQuadwordWords] of TWordOverlay);
    7: (AsLongwordOverlay: array[TQuadwordLongwords] of TLongwordOverlay);
  end;

  TQuadwordBitHelper = type helper(TQWordHelper) for qword
  const
    MinBit      = Low(TQuadwordBits);
    MaxBit      = High(TQuadwordBits);
    MinByte     = Low(TQuadwordBytes);
    MaxByte     = High(TQuadwordBytes);
    MinWord     = Low(TQuadwordWords);
    MaxWord     = High(TQuadwordWords);
    MinLongword = Low(TQuadwordLongwords);
    MaxLongword = High(TQuadwordLongwords);
  public
    procedure Clear;
    function  ToBinString(const HelperFormatSettings: THelperFormatSettings; const aShowLeadingZeros: boolean = true): string; overload;
    function  ToBinString(const aShowLeadingZeros: boolean = true): string; overload;
    function  HighestBitPos: cardinal; inline;
    function  LowestBitPos: cardinal; inline;
    function  BitsCount: byte; inline;
    function  GetBit(const aIndex: TQuadwordBits): boolean;
    procedure SetBit(const aIndex: TQuadwordBits; const NewValue: boolean);
    property  Bit[aIndex: TWordBits]: boolean read GetBit write SetBit;
    function  GetByte(const aIndex: TQuadwordBytes): byte;
    procedure SetByte(const aIndex: TQuadwordBytes; const NewValue: byte);
    property  Byte[aIndex: TQuadwordBytes]: byte read GetByte write SetByte;
    function  GetWord(const aIndex: TQuadwordWords): word;
    procedure SetWord(const aIndex: TQuadwordWords; const NewValue: word);
    property  Word[aIndex: TQuadwordWords]: word read GetWord write SetWord;
    function  GetLongword(const aIndex: TQuadwordLongwords): longword;
    procedure SetLongword(const aIndex: TQuadwordLongwords; const NewValue: longword);
    property  Longword[aIndex: TQuadwordLongwords]: longword read GetLongword write SetLongword;
  end;


implementation

///////////////////////
// TBooleanBitHelper //
///////////////////////

function TBooleanBitHelper.ToString(const aBit: boolean; const aTrueStr, aFalseStr: string; const aCharsCase: TStringCaseFormat = scfUnchangedCase): string; overload;
begin
  if aBit then
    case aCharsCase of
      scfLowerCase:        Result := AnsiLowerCase(aTrueStr);
      scfUpperCase:        Result := AnsiUpperCase(aTrueStr);
    else
      {scfUnchangedCase:}  Result := aTrueStr;
    end
  else
    case aCharsCase of
      scfLowerCase:        Result := AnsiLowerCase(aFalseStr);
      scfUpperCase:        Result := AnsiUpperCase(aFalseStr);
    else
      {scfUnchangedCase:}  Result := aFalseStr;
    end;
end;

function TBooleanBitHelper.ToString(const aTrueStr, aFalseStr: string; const aCharsCase: TStringCaseFormat = scfUnchangedCase): string; overload;
begin
  Result := ToString(Self, aTrueStr, aFalseStr, aCharsCase);
end;

function TBooleanBitHelper.ToOneZeroString: string;
begin
  Result := ToString(BIT_ONE_TXT, BIT_ZERO_TXT);
end;

function TBooleanBitHelper.ToOnOffString(const aCharsCase: TStringCaseFormat = scfUnchangedCase): string;
begin
  Result := ToString(BIT_ON_TXT, BIT_OFF_TXT, aCharsCase);
end;

function TBooleanBitHelper.ToTrueFalseString(const aCharsCase: TStringCaseFormat = scfUnchangedCase): string;
begin
  Result := ToString(BIT_TRUE_TXT, BIT_FALSE_TXT, aCharsCase);
end;

////////////////////
// TByteBitHelper //
////////////////////

procedure TByteBitHelper.Clear;
begin
  Self := 0;
end;

function TByteBitHelper.ToBinString(const HelperFormatSettings: THelperFormatSettings; const aShowLeadingZeros: boolean = true): string; overload;
var
  i: TByteBits;
  LeadingZeros: boolean;
begin
  Result := HelperFormatSettings.PrefixString;
  LeadingZeros := true;
  for i := MaxBit downto MinBit do
  begin
    LeadingZeros := LeadingZeros and not Self.Bit[i];
    if aShowLeadingZeros or (not LeadingZeros) or (i = MinBit) then
    begin
      if (i <> MaxBit) and ((i + 1) mod 4 = 0) then // every 4 bits we have a nibble
        Result += HelperFormatSettings.NibbleSeparator;
      Result += Self.Bit[i].ToOneZeroString;
    end;
  end;
  Result += HelperFormatSettings.SufixString;
end;

function TByteBitHelper.ToBinString(const aShowLeadingZeros: boolean = true): string; overload;
begin
  Result := ToBinString(DefaultHelperFormatSettings, aShowLeadingZeros);
end;

function TByteBitHelper.HighestBitPos: cardinal;
begin
  Result := BsrByte(Self);
end;

function TByteBitHelper.LowestBitPos: cardinal;
begin
  Result := BsfByte(Self);
end;

function TByteBitHelper.BitsCount: byte;
begin
  Result := PopCnt(Self);
end;

function TByteBitHelper.GetBit(const aIndex: TByteBits): boolean;
begin
  // Result := ((Self shr aIndex) and Byte(1)) = Byte(1) // old slower version
  Result := (Self and Byte(BitValueLookupTable[aIndex])) = Byte(BitValueLookupTable[aIndex]);
end;

procedure TByteBitHelper.SetBit(const aIndex: TByteBits; const NewValue: boolean);
begin
  // Self := (Self or (Byte(1) shl aIndex)) xor (Byte(not NewValue) shl aIndex); // old slower version
  if NewValue then
    Self := Self or Byte(BitValueLookupTable[aIndex])
  else
    Self := Self and (not Byte(BitValueLookupTable[aIndex]));
end;

////////////////////
// TWordBitHelper //
////////////////////

procedure TWordBitHelper.Clear;
begin
  Self := 0;
end;

function TWordBitHelper.ToBinString(const HelperFormatSettings: THelperFormatSettings; const aShowLeadingZeros: boolean = true): string; overload;
var
  i: TWordBits;
  LeadingZeros: boolean;
begin
  Result := HelperFormatSettings.PrefixString;
  LeadingZeros := true;
  for i := MaxBit downto MinBit do
  begin
    LeadingZeros := LeadingZeros and not Self.Bit[i];
    if aShowLeadingZeros or (not LeadingZeros) or (i = MinBit) then
    begin
      if (i <> MaxBit) and ((i + 1) mod 8 = 0) then // every 8 bits we have a byte
        Result += HelperFormatSettings.ByteSeparator
      else
        if (i <> MaxBit) and ((i + 1) mod 4 = 0) then // every 4 bits we have a nibble
          Result += HelperFormatSettings.NibbleSeparator;
      Result += Self.Bit[i].ToOneZeroString;
    end;
  end;
  Result += HelperFormatSettings.SufixString;
end;

function TWordBitHelper.ToBinString(const aShowLeadingZeros: boolean = true): string; overload;
begin
  Result := ToBinString(DefaultHelperFormatSettings, aShowLeadingZeros);
end;

function TWordBitHelper.HighestBitPos: cardinal;
begin
  Result := BsrWord(Self);
end;

function TWordBitHelper.LowestBitPos: cardinal;
begin
  Result := BsfWord(Self);
end;

function TWordBitHelper.BitsCount: byte;
begin
  Result := PopCnt(Self);
end;

function TWordBitHelper.GetBit(const aIndex: TWordBits): boolean;
begin
  // Result := Self.Byte[aIndex div 8].Bit[aIndex mod 8] // old slower version
  Result := (Self and Word(BitValueLookupTable[aIndex])) = Word(BitValueLookupTable[aIndex]);
end;

procedure TWordBitHelper.SetBit(const aIndex: TWordBits; const NewValue: boolean);
begin
  // Self := (Self or (Word(1) shl aIndex)) xor (Word(not NewValue) shl aIndex); // old slower version
  if NewValue then
    Self := Self or Word(BitValueLookupTable[aIndex])
  else
    Self := Self and (not Word(BitValueLookupTable[aIndex]));
end;

function TWordBitHelper.GetByte(const aIndex: TWordBytes): byte;
begin
  Result := TWordOverlay(Self).AsByte[aIndex]
end;

procedure TWordBitHelper.SetByte(const aIndex: TWordBytes; const NewValue: byte);
begin
  TWordOverlay(Self).AsByte[aIndex] := NewValue;
end;

////////////////////////
// TLongwordBitHelper //
////////////////////////

procedure TLongwordBitHelper.Clear;
begin
  Self := 0;
end;

function TLongwordBitHelper.ToBinString(const HelperFormatSettings: THelperFormatSettings; const aShowLeadingZeros: boolean = true): string; overload;
var
  i: TLongwordBits;
  LeadingZeros: boolean;
begin
  Result := HelperFormatSettings.PrefixString;
  LeadingZeros := true;
  for i := MaxBit downto MinBit do
  begin
    LeadingZeros := LeadingZeros and not Self.Bit[i];
    if aShowLeadingZeros or (not LeadingZeros) or (i = MinBit) then
    begin
      if (i <> MaxBit) and ((i + 1) mod 16 = 0) then // every 16 bits we have a word
        Result += HelperFormatSettings.WordSeparator
      else
        if (i <> MaxBit) and ((i + 1) mod 8 = 0) then // every 8 bits we have a byte
          Result += HelperFormatSettings.ByteSeparator
        else
          if (i <> MaxBit) and ((i + 1) mod 4 = 0) then // every 4 bits we have a nibble
            Result += HelperFormatSettings.NibbleSeparator;
      Result += Self.Bit[i].ToOneZeroString;
    end;
  end;
  Result += HelperFormatSettings.SufixString;
end;

function TLongwordBitHelper.ToBinString(const aShowLeadingZeros: boolean = true): string; overload;
begin
  Result := ToBinString(DefaultHelperFormatSettings, aShowLeadingZeros);
end;

function  TLongwordBitHelper.HighestBitPos: cardinal;
begin
  Result := BsrDWord(Self);
end;

function  TLongwordBitHelper.LowestBitPos: cardinal;
begin
  Result := BsfDWord(Self);
end;

function TLongwordBitHelper.BitsCount: byte;
begin
  Result := PopCnt(Self);
end;

function TLongwordBitHelper.GetBit(const aIndex: TLongwordBits): boolean;
begin
  // Result := Self.Byte[aIndex div 8].Bit[aIndex mod 8] // old slower version
  Result := (Self and Longword(BitValueLookupTable[aIndex])) = Longword(BitValueLookupTable[aIndex]);
end;

procedure TLongwordBitHelper.SetBit(const aIndex: TLongwordBits; const NewValue: boolean);
begin
  // Self := (Self or (Longword(1) shl aIndex)) xor (Longword(not NewValue) shl aIndex); // old slower version
  if NewValue then
    Self := Self or Longword(BitValueLookupTable[aIndex])
  else
    Self := Self and (not Longword(BitValueLookupTable[aIndex]));
end;

function TLongwordBitHelper.GetByte(const aIndex: TLongwordBytes): byte;
begin
  Result := TLongwordOverlay(Self).AsByte[aIndex]
end;

procedure TLongwordBitHelper.SetByte(const aIndex: TLongwordBytes; const NewValue: byte);
begin
  TLongwordOverlay(Self).AsByte[aIndex] := NewValue;
end;

function TLongwordBitHelper.GetWord(const aIndex: TLongwordWords): word;
begin
  Result := TLongwordOverlay(Self).AsWord[aIndex]
end;

procedure TLongwordBitHelper.SetWord(const aIndex: TLongwordWords; const NewValue: word);
begin
  TLongwordOverlay(Self).AsWord[aIndex] := NewValue;
end;

////////////////////////
// TQuadwordBitHelper //
////////////////////////

procedure TQuadwordBitHelper.Clear;
begin
  Self := 0;
end;

function TQuadwordBitHelper.ToBinString(const HelperFormatSettings: THelperFormatSettings; const aShowLeadingZeros: boolean = true): string; overload;
var
  i: TQuadwordBits;
  LeadingZeros: boolean;
begin
  Result := HelperFormatSettings.PrefixString;
  LeadingZeros := true;
  for i := MaxBit downto MinBit do
  begin
    LeadingZeros := LeadingZeros and not Self.Bit[i];
    if aShowLeadingZeros or (not LeadingZeros) or (i = MinBit) then
    begin
      if (i <> MaxBit) and ((i + 1) mod 32 = 0) then // every 32 bits we have a longword
        Result += HelperFormatSettings.LongwordSeparator
      else
        if (i <> MaxBit) and ((i + 1) mod 16 = 0) then // every 16 bits we have a word
          Result += HelperFormatSettings.WordSeparator
        else
          if (i <> MaxBit) and ((i + 1) mod 8 = 0) then // every 8 bits we have a byte
            Result += HelperFormatSettings.ByteSeparator
          else
            if (i <> MaxBit) and ((i + 1) mod 4 = 0) then // every 4 bits we have a nibble
              Result += HelperFormatSettings.NibbleSeparator;
      Result += Self.Bit[i].ToOneZeroString;
    end;
  end;
  Result += HelperFormatSettings.SufixString;
end;

function TQuadwordBitHelper.ToBinString(const aShowLeadingZeros: boolean = true): string; overload;
begin
  Result := ToBinString(DefaultHelperFormatSettings, aShowLeadingZeros);
end;

function  TQuadwordBitHelper.HighestBitPos: cardinal;
begin
  Result := BsrQWord(Self);
end;

function  TQuadwordBitHelper.LowestBitPos: cardinal;
begin
  Result := BsfQWord(Self);
end;

function TQuadwordBitHelper.BitsCount: byte;
begin
  Result := PopCnt(Self);
end;


function TQuadwordBitHelper.GetBit(const aIndex: TQuadwordBits): boolean;
begin
  // Result := Self.Byte[aIndex div 8].Bit[aIndex mod 8] // old slower version
  Result := (Self and BitValueLookupTable[aIndex]) = BitValueLookupTable[aIndex];
end;

procedure TQuadwordBitHelper.SetBit(const aIndex: TQuadwordBits; const NewValue: boolean);
begin
  if aIndex in [MinBit..MaxBit] then
    Self := (Self or (qword(1) shl aIndex)) xor (qword(not NewValue) shl aIndex);
end;

function TQuadwordBitHelper.GetByte(const aIndex: TQuadwordBytes): byte;
begin
  Result := TQuadwordOverlay(Self).AsByte[aIndex]
end;

procedure TQuadwordBitHelper.SetByte(const aIndex: TQuadwordBytes; const NewValue: byte);
begin
  TQuadwordOverlay(Self).AsByte[aIndex] := NewValue;
end;

function TQuadwordBitHelper.GetWord(const aIndex: TQuadwordWords): word;
begin
  Result := TQuadwordOverlay(Self).AsWord[aIndex]
end;

procedure TQuadwordBitHelper.SetWord(const aIndex: TQuadwordWords; const NewValue: word);
begin
  TQuadwordOverlay(Self).AsWord[aIndex] := NewValue;
end;


function TQuadwordBitHelper.GetLongword(const aIndex: TQuadwordLongwords): longword;
begin
  Result := TQuadwordOverlay(Self).AsLongword[aIndex]
end;

procedure TQuadwordBitHelper.SetLongword(const aIndex: TQuadwordLongwords; const NewValue: longword);
begin
  TQuadwordOverlay(Self).AsLongword[aIndex] := NewValue;
end;

end.

